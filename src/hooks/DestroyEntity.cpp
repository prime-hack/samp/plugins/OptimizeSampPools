#include <vector>

#include <MinHook.h>

#include "CPools.h"

namespace PedPool {
	extern void reset_id( ptrdiff_t index );
} // namespace PedPool

namespace VehiclePool {
	extern void reset_id( ptrdiff_t index );
} // namespace VehiclePool

namespace ObjectPool {
	extern void reset_id( ptrdiff_t index );
} // namespace ObjectPool

namespace {

	void *oDestroyEntity = nullptr;
	void __fastcall hkDestroyEntity( void *entity, void *edx ) {
		auto index = CPools::GetPedPool()->GetIndex( static_cast<CPed *>( entity ) );
		if ( index >= 0 )
			PedPool::reset_id( index );
		else {
			index = CPools::GetVehiclePool()->GetIndex( static_cast<CVehicle *>( entity ) );
			if ( index >= 0 )
				VehiclePool::reset_id( index );
			else {
				index = CPools::GetObjectPool()->GetIndex( static_cast<CObject *>( entity ) );
				if ( index >= 0 ) ObjectPool::reset_id( index );
			}
		}

		return reinterpret_cast<decltype( hkDestroyEntity ) *>( oDestroyEntity )( entity, edx );
	}

	const auto address = reinterpret_cast<void *>( 0x00535E90 );
} // namespace

namespace DestroyEntity {
	void install() {
		MH_CreateHook( address, reinterpret_cast<void *>( hkDestroyEntity ), reinterpret_cast<void **>( &oDestroyEntity ) );
		MH_EnableHook( address );
	}

	void remove() {
		MH_DisableHook( address );
		MH_RemoveHook( address );
	}
} // namespace DestroyEntity