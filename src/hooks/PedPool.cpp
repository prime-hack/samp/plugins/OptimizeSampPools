#include "PedPool.h"

#include <fstream>
#include <vector>

#include <MinHook.h>

#include "CPools.h"
#include "samp.h"

#ifdef _DEBUG
extern std::ofstream plugin_log;
#endif

namespace {
	std::vector<OptimizeSampLoops::ped_id_t> ped_ids;
	OptimizeSampLoops::ped_id_t ped_id;

	struct remote_player_data_t {
		union {
			struct {
				char pad[8];
				OptimizeSampLoops::ped_id_t player_id;
			} r3;
#pragma pack( push, 1 )
			struct {
				char pad[0xAB];
				OptimizeSampLoops::ped_id_t player_id;
			} r1;
#pragma pack( pop )
		};
	};
	struct player_t {
		char pad[0x2a4];
		CPed *gta_ped;
	};
	struct actor_data_t {
		OptimizeSampLoops::ped_id_t actor_id;
	};
	struct actor_t {
		char pad[0x48];
		CPed *gta_ped;
	};

	void *oFindPlayerId = nullptr;
	OptimizeSampLoops::ped_id_t __fastcall hkFindPlayerId( void *pool, void *edx, void *ped ) {
		return OptimizeSampLoops::get_ped_id( ped );
	}

	void *oAddPlayerToWorld = nullptr;
	int __fastcall hkAddPlayerToWorld( remote_player_data_t *self,
									   void *edx,
									   uint8_t team,
									   int32_t skinId,
									   int _,
									   float *pos,
									   float angle,
									   uint32_t color,
									   uint8_t fight_style ) {
		ped_id = samp::is_r1() ? self->r1.player_id : self->r3.player_id;
		return reinterpret_cast<decltype( hkAddPlayerToWorld ) *>(
			oAddPlayerToWorld )( self, edx, team, skinId, _, pos, angle, color, fight_style );
	}

	void *oCreatePlayer = nullptr;
	player_t *__fastcall hkCreatePlayer( void *self,
										 void *edx,
										 int32_t skinId,
										 float x,
										 float y,
										 float z,
										 float angle,
										 int _,
										 int showNametag ) {
		auto *ped = reinterpret_cast<decltype( hkCreatePlayer ) *>( oCreatePlayer )( self, edx, skinId, x, y, z, angle, _, showNametag );
		if ( ped == nullptr ) return ped;

		const auto index = CPools::GetPedPool()->GetIndex( ped->gta_ped );
		if ( index < 0 ) return ped;

		if ( index >= ped_ids.size() ) ped_ids.resize( index + 1, OptimizeSampLoops::INVALID_PED_ID );
		ped_ids[index] = ped_id;

		return ped;
	}

	void *oFindActorId = nullptr;
	OptimizeSampLoops::ped_id_t __fastcall hkFindActorId( void *pool, void *edx, void *ped ) {
		return OptimizeSampLoops::get_ped_id( ped );
	}

	void *oAddActorToWorld = nullptr;
	int __fastcall hkAddActorToWorld( void *pool, void *edx, actor_data_t *data ) {
		ped_id = data->actor_id;
		return reinterpret_cast<decltype( hkAddActorToWorld ) *>( oAddActorToWorld )( pool, edx, data );
	}

	void *oCreateActor = nullptr;
	actor_t *__fastcall hkCreateActor( actor_t *self, void *edx, int32_t skinId, float x, float y, float z, float angle ) {
		auto *ped = reinterpret_cast<decltype( hkCreateActor ) *>( oCreateActor )( self, edx, skinId, x, y, z, angle );
		if ( ped == nullptr ) return ped;

		const auto index = CPools::GetPedPool()->GetIndex( ped->gta_ped );
		if ( index < 0 ) return ped;

		if ( index >= ped_ids.size() ) ped_ids.resize( index + 1, OptimizeSampLoops::INVALID_PED_ID );
		ped_ids[index] = ped_id;

		return ped;
	}

	struct addresses_t {
		void *FindPlayerId;
		void *AddPlayerToWorld;
		void *CreatePlayer;
		void *FindActorId;
		void *AddActorToWorld;
		void *CreateActor;
		addresses_t() {
			FindPlayerId = samp::is_r1() ? samp::address_of( 0x10010420 ) : samp::address_of( 0x10013570 );
			AddPlayerToWorld = samp::is_r1() ? samp::address_of( 0x10013890 ) : samp::address_of( 0x10016A90 );
			CreatePlayer = samp::is_r1() ? samp::address_of( 0x1009B750 ) : samp::address_of( 0x1009FA00 );
			FindActorId = samp::address_of( 0x100018A0 );
			AddActorToWorld = samp::address_of( 0x100018F0 );
			CreateActor = samp::is_r1() ? samp::address_of( 0x10097C60 ) : samp::address_of( 0x1009BBA0 );
#ifdef _DEBUG
			plugin_log << "FindPlayerId address: " << std::hex << FindPlayerId << std::endl;
			plugin_log << "AddPlayerToWorld address: " << std::hex << AddPlayerToWorld << std::endl;
			plugin_log << "CreatePlayer address: " << std::hex << CreatePlayer << std::endl;
			plugin_log << "FindActorId address: " << std::hex << FindActorId << std::endl;
			plugin_log << "AddActorToWorld address: " << std::hex << AddActorToWorld << std::endl;
			plugin_log << "CreateActor address: " << std::hex << CreateActor << std::endl;
#endif
		}
	};
} // namespace

OptimizeSampLoops::ped_id_t OptimizeSampLoops__get_ped_id( void *ped ) {
	const auto index = CPools::GetPedPool()->GetIndex( static_cast<CPed *>( ped ) );

	if ( index >= 0 && index < ped_ids.size() ) return ped_ids[index];

	return OptimizeSampLoops::INVALID_PED_ID;
}

namespace PedPool {
	void install() {
		addresses_t addresses;

		MH_CreateHook( addresses.FindPlayerId, reinterpret_cast<void *>( &hkFindPlayerId ), reinterpret_cast<void **>( &oFindPlayerId ) );
		MH_CreateHook( addresses.AddPlayerToWorld,
					   reinterpret_cast<void *>( &hkAddPlayerToWorld ),
					   reinterpret_cast<void **>( &oAddPlayerToWorld ) );
		MH_CreateHook( addresses.CreatePlayer, reinterpret_cast<void *>( &hkCreatePlayer ), reinterpret_cast<void **>( &oCreatePlayer ) );
		MH_CreateHook( addresses.FindActorId, reinterpret_cast<void *>( &hkFindActorId ), reinterpret_cast<void **>( &oFindActorId ) );
		MH_CreateHook( addresses.AddActorToWorld,
					   reinterpret_cast<void *>( &hkAddActorToWorld ),
					   reinterpret_cast<void **>( &oAddActorToWorld ) );
		MH_CreateHook( addresses.CreateActor, reinterpret_cast<void *>( &hkCreateActor ), reinterpret_cast<void **>( &oCreateActor ) );

#ifdef _DEBUG
		plugin_log << "Create PedPool hooks" << std::endl;
#endif

		MH_QueueEnableHook( addresses.FindPlayerId );
		MH_QueueEnableHook( addresses.AddPlayerToWorld );
		MH_QueueEnableHook( addresses.CreatePlayer );
		MH_QueueEnableHook( addresses.FindActorId );
		MH_QueueEnableHook( addresses.AddActorToWorld );
		MH_QueueEnableHook( addresses.CreateActor );

		MH_ApplyQueued();

#ifdef _DEBUG
		plugin_log << "Enable PedPool hooks" << std::endl;
#endif
	}

	void remove() {
		addresses_t addresses;

		MH_QueueDisableHook( addresses.FindPlayerId );
		MH_QueueDisableHook( addresses.AddPlayerToWorld );
		MH_QueueDisableHook( addresses.CreatePlayer );
		MH_QueueDisableHook( addresses.FindActorId );
		MH_QueueDisableHook( addresses.AddActorToWorld );
		MH_QueueDisableHook( addresses.CreateActor );

		MH_ApplyQueued();

#ifdef _DEBUG
		plugin_log << "Disable PedPool hooks" << std::endl;
#endif

		MH_RemoveHook( addresses.FindPlayerId );
		MH_RemoveHook( addresses.AddPlayerToWorld );
		MH_RemoveHook( addresses.CreatePlayer );
		MH_RemoveHook( addresses.FindActorId );
		MH_RemoveHook( addresses.AddActorToWorld );
		MH_RemoveHook( addresses.CreateActor );

#ifdef _DEBUG
		plugin_log << "Remove PedPool hooks" << std::endl;
#endif
	}

	void reset_id( ptrdiff_t index ) {
		if ( index >= 0 && index < ped_ids.size() ) ped_ids[index] = OptimizeSampLoops::INVALID_PED_ID;
	}
} // namespace PedPool
