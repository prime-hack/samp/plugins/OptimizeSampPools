#include "VehiclePool.h"

#include <fstream>
#include <vector>

#include <MinHook.h>

#include "CPools.h"
#include "samp.h"

#ifdef _DEBUG
extern std::ofstream plugin_log;
#endif

namespace {
	std::vector<OptimizeSampLoops::vehicle_id_t> vehicle_ids;
	OptimizeSampLoops::vehicle_id_t vehicle_id;

	struct vehicle_data_t {
		OptimizeSampLoops::vehicle_id_t vehicle_id;
	};
	struct vehicle_t {
		char pad[0x4C];
		CVehicle *gta_vehicle;
	};

	void *oFindVehicleId = nullptr;
	OptimizeSampLoops::vehicle_id_t __fastcall hkFindVehicleId( void *pool, void *edx, void *vehicle ) {
		return OptimizeSampLoops::get_vehicle_id( vehicle );
	}

	void *oCreateVehicle = nullptr;
	int __fastcall hkCreateVehicle( void *pool, void *edx, vehicle_data_t *data ) {
		vehicle_id = data->vehicle_id;
		return reinterpret_cast<decltype( hkCreateVehicle ) *>( oCreateVehicle )( pool, edx, data );
	}

	void *oNewVehicle = nullptr;
	vehicle_t *__fastcall hkNewVehicle( vehicle_t *self,
										void *edx,
										int32_t modelId,
										float x,
										float y,
										float z,
										float angle,
										int32_t modelOverride,
										int siren ) {
		auto *vehicle =
			reinterpret_cast<decltype( hkNewVehicle ) *>( oNewVehicle )( self, edx, modelId, x, y, z, angle, modelOverride, siren );
		if ( vehicle == nullptr ) return vehicle;

		const auto index = CPools::GetVehiclePool()->GetIndex( vehicle->gta_vehicle );
		if ( index < 0 ) return vehicle;

		if ( index >= vehicle_ids.size() ) vehicle_ids.resize( index + 1, OptimizeSampLoops::INVALID_VEHICLE_ID );
		vehicle_ids[index] = vehicle_id;

		return vehicle;
	}

	struct addresses_t {
		void *FindVehicleId;
		void *CreateVehicle;
		void *NewVehicle;
		addresses_t() {
			FindVehicleId = samp::is_r1() ? samp::address_of( 0x1001B0A0 ) : samp::address_of( 0x1001E440 );
			CreateVehicle = samp::is_r1() ? samp::address_of( 0x1001B590 ) : samp::address_of( 0x1001E930 );
			NewVehicle = samp::is_r1() ? samp::address_of( 0x100B1E70 ) : samp::address_of( 0x100B7B30 );
#ifdef _DEBUG
			plugin_log << "FindVehicleId address: " << std::hex << FindVehicleId << std::endl;
			plugin_log << "CreateVehicle address: " << std::hex << CreateVehicle << std::endl;
			plugin_log << "NewVehicle address: " << std::hex << NewVehicle << std::endl;
#endif
		}
	};
} // namespace

OptimizeSampLoops::vehicle_id_t OptimizeSampLoops__get_vehicle_id( void *vehicle ) {
	auto index = CPools::GetVehiclePool()->GetIndex( static_cast<CVehicle *>( vehicle ) );

	if ( index >= 0 && index < vehicle_ids.size() ) return vehicle_ids[index];

	return OptimizeSampLoops::INVALID_VEHICLE_ID;
}

namespace VehiclePool {
	void install() {
		addresses_t addresses;
		MH_CreateHook( addresses.FindVehicleId, reinterpret_cast<void *>( hkFindVehicleId ), reinterpret_cast<void **>( &oFindVehicleId ) );
		MH_CreateHook( addresses.CreateVehicle, reinterpret_cast<void *>( hkCreateVehicle ), reinterpret_cast<void **>( &oCreateVehicle ) );
		MH_CreateHook( addresses.NewVehicle, reinterpret_cast<void *>( hkNewVehicle ), reinterpret_cast<void **>( &oNewVehicle ) );

#ifdef _DEBUG
		plugin_log << "Create VehiclePool hooks" << std::endl;
#endif

		MH_QueueEnableHook( addresses.FindVehicleId );
		MH_QueueEnableHook( addresses.CreateVehicle );
		MH_QueueEnableHook( addresses.NewVehicle );

		MH_ApplyQueued();

#ifdef _DEBUG
		plugin_log << "Enable VehiclePool hooks" << std::endl;
#endif
	}

	void remove() {
		addresses_t addresses;
		MH_QueueDisableHook( addresses.FindVehicleId );
		MH_QueueDisableHook( addresses.CreateVehicle );
		MH_QueueDisableHook( addresses.NewVehicle );

		MH_ApplyQueued();

#ifdef _DEBUG
		plugin_log << "Disable VehiclePool hooks" << std::endl;
#endif

		MH_RemoveHook( addresses.FindVehicleId );
		MH_RemoveHook( addresses.CreateVehicle );
		MH_RemoveHook( addresses.NewVehicle );

#ifdef _DEBUG
		plugin_log << "Remove VehiclePool hooks" << std::endl;
#endif
	}

	void reset_id( ptrdiff_t index ) {
		if ( index >= 0 && index < vehicle_ids.size() ) vehicle_ids[index] = OptimizeSampLoops::INVALID_VEHICLE_ID;
	}
} // namespace VehiclePool