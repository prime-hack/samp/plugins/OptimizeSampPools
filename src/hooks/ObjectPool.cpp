#include "ObjectPool.h"

#include <fstream>
#include <vector>

#include <MinHook.h>

#include "CPools.h"
#include "samp.h"

#ifdef _DEBUG
extern std::ofstream plugin_log;
#endif

namespace {
	std::vector<OptimizeSampLoops::object_id_t> object_ids;
	OptimizeSampLoops::object_id_t object_id;

	struct object_t {
		char pad[0x40];
		CObject *gta_object;
	};
	struct object_pool_t {
		char pad[0xFA4];
		object_t *objects[1000];
	};

	void *oFindObjectId = nullptr;
	OptimizeSampLoops::object_id_t __fastcall hkFindObjectId( void *pool, void *edx, void *object ) {
		return OptimizeSampLoops::get_object_id( object );
	}

	void *oCreateObject = nullptr;
	int __fastcall hkCreateObject( void *pool,
								   void *edx,
								   OptimizeSampLoops::object_id_t id,
								   int32_t modelId,
								   float x,
								   float y,
								   float z,
								   float rx,
								   float ry,
								   float rz,
								   int lod_distance ) {
		object_id = id;
		return reinterpret_cast<decltype( hkCreateObject ) *>( oCreateObject )( pool, edx, id, modelId, x, y, z, rx, ry, rz, lod_distance );
	}

	void *oNewObject = nullptr;
	object_t *__fastcall hkNewObject( void *game,
									  void *edx,
									  int32_t modelId,
									  float x,
									  float y,
									  float z,
									  float rx,
									  float ry,
									  float rz,
									  int lod_distance,
									  int unk,
									  int unk2 ) {
		auto *object =
			reinterpret_cast<decltype( hkNewObject ) *>( oNewObject )( game, edx, modelId, x, y, z, rx, ry, rz, lod_distance, unk, unk2 );
		if ( object == nullptr ) return object;

		const auto index = CPools::GetObjectPool()->GetIndex( object->gta_object );
		if ( index < 0 ) return object;

		if ( index >= object_ids.size() ) object_ids.resize( index + 1, OptimizeSampLoops::INVALID_OBJECT_ID );
		object_ids[index] = object_id;

		return object;
	}

	void *oFindSampObject = nullptr;
	object_t *__fastcall hkFindSampObject( object_pool_t *pool, void *edx, void *gta_object ) {
		const auto object_id = OptimizeSampLoops::get_object_id( gta_object );
		if ( object_id == OptimizeSampLoops::INVALID_OBJECT_ID ) return nullptr;

		return pool->objects[object_id];
	}

	struct addresses_t {
		void *FindObjectId;
		void *CreateObject;
		void *NewObject;
		void *FindSampObject;
		addresses_t() {
			FindObjectId = samp::is_r1() ? samp::address_of( 0x1000F560 ) : samp::address_of( 0x100126C0 );
			CreateObject = samp::is_r1() ? samp::address_of( 0x1000F470 ) : samp::address_of( 0x10012580 );
			NewObject = samp::is_r1() ? samp::address_of( 0x1009B970 ) : samp::address_of( 0x1009FC20 );
			FindSampObject = samp::is_r1() ? samp::address_of( 0x1000F521 ) : samp::address_of( 0x10012680 );
#ifdef _DEBUG
			plugin_log << "FindObjectId address: " << std::hex << FindObjectId << std::endl;
			plugin_log << "CreateObject address: " << std::hex << CreateObject << std::endl;
			plugin_log << "NewObject address: " << std::hex << NewObject << std::endl;
			plugin_log << "FindSampObject address: " << std::hex << FindSampObject << std::endl;
#endif
		}
	};
} // namespace

OptimizeSampLoops::object_id_t OptimizeSampLoops__get_object_id( void *object ) {
	auto index = CPools::GetObjectPool()->GetIndex( static_cast<CObject *>( object ) );

	if ( index >= 0 && index < object_ids.size() ) return object_ids[index];

	return OptimizeSampLoops::INVALID_OBJECT_ID;
}

namespace ObjectPool {
	void install() {
		addresses_t addresses;
		MH_CreateHook( addresses.FindObjectId, reinterpret_cast<void *>( hkFindObjectId ), reinterpret_cast<void **>( &oFindObjectId ) );
		MH_CreateHook( addresses.CreateObject, reinterpret_cast<void *>( hkCreateObject ), reinterpret_cast<void **>( &oCreateObject ) );
		MH_CreateHook( addresses.NewObject, reinterpret_cast<void *>( hkNewObject ), reinterpret_cast<void **>( &oNewObject ) );
		MH_CreateHook( addresses.FindSampObject,
					   reinterpret_cast<void *>( hkFindSampObject ),
					   reinterpret_cast<void **>( &oFindSampObject ) );

#ifdef _DEBUG
		plugin_log << "Create ObjectPool hooks" << std::endl;
#endif

		MH_QueueEnableHook( addresses.FindObjectId );
		MH_QueueEnableHook( addresses.CreateObject );
		MH_QueueEnableHook( addresses.NewObject );
		MH_QueueEnableHook( addresses.FindSampObject );

		MH_ApplyQueued();

#ifdef _DEBUG
		plugin_log << "Enable ObjectPool hooks" << std::endl;
#endif
	}

	void remove() {
		addresses_t addresses;
		MH_QueueDisableHook( addresses.FindObjectId );
		MH_QueueDisableHook( addresses.CreateObject );
		MH_QueueDisableHook( addresses.NewObject );
		MH_QueueDisableHook( addresses.FindSampObject );

		MH_ApplyQueued();

#ifdef _DEBUG
		plugin_log << "Disable ObjectPool hooks" << std::endl;
#endif

		MH_RemoveHook( addresses.FindObjectId );
		MH_RemoveHook( addresses.CreateObject );
		MH_RemoveHook( addresses.NewObject );
		MH_RemoveHook( addresses.FindSampObject );

#ifdef _DEBUG
		plugin_log << "Remove ObjectPool hooks" << std::endl;
#endif
	}

	void reset_id( ptrdiff_t index ) {
		if ( index >= 0 && index < object_ids.size() ) object_ids[index] = OptimizeSampLoops::INVALID_OBJECT_ID;
	}
} // namespace ObjectPool