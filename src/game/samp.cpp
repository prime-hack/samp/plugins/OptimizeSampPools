#include "samp.h"

#include <cstdint>

#include <windows.h>

static uintptr_t Library() {
	static uintptr_t library = 0;
	if ( library != 0 ) return library;

	auto *hModule = GetModuleHandleA( "samp" );
	if ( hModule == nullptr || hModule == INVALID_HANDLE_VALUE ) return 0;

	library = reinterpret_cast<uintptr_t>( hModule );

	return library;
}

bool samp::is_r1() noexcept {
	const auto *ptr = address_of( 0x129 );
	if ( ptr == nullptr ) return false;
	return *static_cast<const uint8_t *>( ptr ) == 0xF4;
}

void *samp::address_of( ptrdiff_t offset ) noexcept {
	const auto library = Library();
	if ( library == 0 ) return nullptr;

	return reinterpret_cast<void *>( library + ( offset & 0x00'ff'ff'ff ) );
}
