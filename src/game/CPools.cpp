#include "CPools.h"

#define DECL_STATIC( static_field, ... ) \
	decltype( static_field ) static_field { \
		__VA_ARGS__ \
	}

DECL_STATIC( CPools::ms_pPedPool, *reinterpret_cast<CPools::CPedPool **>( 0x00B74490 ) );
DECL_STATIC( CPools::ms_pVehiclePool, *reinterpret_cast<CPools::CVehiclePool **>( 0x00B74494 ) );
DECL_STATIC( CPools::ms_pObjectPool, *reinterpret_cast<CPools::CObjectPool **>( 0x00B7449C ) );

#undef DECL_STATIC
