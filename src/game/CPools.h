#pragma once

#include "CPool.hpp"

class CPed;
class CVehicle;
class CObject;

class CPools {
	static constexpr auto PED_STRUCT_MAX_SIZE = 0x7C4; // CCopPed
	static constexpr auto VEHICLE_STRUCT_MAX_SIZE = 0xA18; // CHeli
	static constexpr auto OBJECT_STRUCT_MAX_SIZE = 0x19C; // CAnimatedObject

	using CPedPool = CPool<CPed *, PED_STRUCT_MAX_SIZE>;
	using CVehiclePool = CPool<CVehicle *, VEHICLE_STRUCT_MAX_SIZE>;
	using CObjectPool = CPool<CObject *, OBJECT_STRUCT_MAX_SIZE>;

	static CPedPool *&ms_pPedPool;
	static CVehiclePool *&ms_pVehiclePool;
	static CObjectPool *&ms_pObjectPool;

public:
	static decltype( ms_pPedPool ) GetPedPool() { return ms_pPedPool; }
	static decltype( ms_pVehiclePool ) GetVehiclePool() { return ms_pVehiclePool; }
	static decltype( ms_pObjectPool ) GetObjectPool() { return ms_pObjectPool; }
};
