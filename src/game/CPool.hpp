#pragma once

#include <cstddef>
#include <cstdint>
#include <type_traits>

template<typename StoreTypePtr, ptrdiff_t MaxSize = sizeof( StoreTypePtr )> struct CPool {
	static_assert( std::is_pointer_v<StoreTypePtr>, "Store type must be a pointer type to work with forward declarations" );
	struct flag_t {
		std::uint8_t id		 : 7;
		std::uint8_t bIsFree : 1;
	};

	StoreTypePtr objects;
	flag_t *flags;
	uint32_t size;
	// ... other fields not interested in here

	ptrdiff_t GetIndex( StoreTypePtr object ) {
		if ( object < objects ) return -1; // Not from pool

		const auto ptrdist = reinterpret_cast<intptr_t>( object ) - reinterpret_cast<intptr_t>( objects );
		if ( ptrdist % MaxSize != 0 ) return -1; // Invalid pointer alignment

		const auto index = ptrdist / MaxSize;
		if ( index >= size || flags[index].bIsFree ) return -1; // Invalid index or object is deleted

		return index;
	}
};