#pragma once

#include <cstddef>

namespace samp {
	[[nodiscard]] bool is_r1() noexcept;
	[[nodiscard]] void *address_of( ptrdiff_t offset ) noexcept;
} // namespace samp
