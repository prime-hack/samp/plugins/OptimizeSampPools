#include <fstream>

#include <MinHook.h>

namespace PedPool {
	extern void install();
	extern void remove();
} // namespace PedPool

namespace VehiclePool {
	extern void install();
	extern void remove();
} // namespace VehiclePool

namespace ObjectPool {
	extern void install();
	extern void remove();
} // namespace ObjectPool

namespace DestroyEntity {
	extern void install();
	extern void remove();
} // namespace DestroyEntity

#ifdef _DEBUG
std::ofstream plugin_log;
#endif

namespace {
	struct loader {
		loader() {
#ifdef _DEBUG
			if ( !plugin_log.is_open() ) plugin_log.open( "OptimizeSampPools.log" );
			plugin_log << "OptimizeSampPools plugin loaded" << std::endl;
#endif
			MH_Initialize();

			PedPool::install();
			VehiclePool::install();
			ObjectPool::install();
			DestroyEntity::install();
		}

		~loader() {
			DestroyEntity::remove();
			ObjectPool::remove();
			VehiclePool::remove();
			PedPool::remove();

			MH_Uninitialize();
#ifdef _DEBUG
			plugin_log << "OptimizeSampPools plugin unloaded" << std::endl;
#endif
		}
	} loader;
} // namespace
