#pragma once

#if __cplusplus
#	ifdef OptimizeSampLoops_LIBRARY
#		define DLL_PUBLIC extern "C" __declspec( dllexport )
#	else
#		define DLL_PUBLIC extern "C" __declspec( dllimport )
#	endif
#else
#	ifdef OptimizeSampLoops_LIBRARY
#		define DLL_PUBLIC __declspec( dllexport )
#	else
#		define DLL_PUBLIC __declspec( dllimport )
#	endif
#endif