#pragma once

#include <stdint.h> // NOLINT(*-deprecated-headers)

#include "export.h"

#if __cplusplus

namespace OptimizeSampLoops {
	using vehicle_id_t = int16_t;
	static constexpr vehicle_id_t INVALID_VEHICLE_ID = -1;
} // namespace OptimizeSampLoops

DLL_PUBLIC OptimizeSampLoops::vehicle_id_t OptimizeSampLoops__get_vehicle_id( void *vehicle );

namespace OptimizeSampLoops {
	/**
	 * Get ID of a game vehicle
	 * @param vehicle pointer to CVehicle
	 * @return ID of vehicle
	 */
	inline vehicle_id_t get_vehicle_id( void *vehicle ) {
		return OptimizeSampLoops__get_vehicle_id( vehicle );
	}
} // namespace OptimizeSampLoops

#else

typedef int16_t vehicle_id_t;
#	define INVALID_VEHICLE_ID ( ( vehicle_id_t ) - 1 )

DLL_PUBLIC vehicle_id_t OptimizeSampLoops__get_vehicle_id( void *vehicle );

#endif