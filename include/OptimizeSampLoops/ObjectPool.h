#pragma once

#include <stdint.h> // NOLINT(*-deprecated-headers)

#include "export.h"

#if __cplusplus

namespace OptimizeSampLoops {
	using object_id_t = int16_t;
	static constexpr object_id_t INVALID_OBJECT_ID = -1;
} // namespace OptimizeSampLoops

DLL_PUBLIC OptimizeSampLoops::object_id_t OptimizeSampLoops__get_object_id( void *object );

namespace OptimizeSampLoops {
	/**
	 * Get ID of a game object
	 * @param object pointer to CObject
	 * @return ID of object
	 */
	inline object_id_t get_object_id( void *object ) {
		return OptimizeSampLoops__get_object_id( object );
	}
} // namespace OptimizeSampLoops

#else

typedef int16_t object_id_t;
#	define INVALID_OBJECT_ID ( ( object_id_t ) - 1 )

DLL_PUBLIC object_id_t OptimizeSampLoops__get_object_id( void *object );

#endif