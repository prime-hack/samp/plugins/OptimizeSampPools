#pragma once

#include <stdint.h> // NOLINT(*-deprecated-headers)

#include "export.h"

#if __cplusplus

namespace OptimizeSampLoops {
	using ped_id_t = int16_t;
	static constexpr ped_id_t INVALID_PED_ID = -1;
} // namespace OptimizeSampLoops

DLL_PUBLIC OptimizeSampLoops::ped_id_t OptimizeSampLoops__get_ped_id( void *ped );

namespace OptimizeSampLoops {
	/**
	 * Get ID of a game ped
	 * @param ped pointer to CPed
	 * @return ID of player or actor
	 */
	inline ped_id_t get_ped_id( void *ped ) {
		return OptimizeSampLoops__get_ped_id( ped );
	}
} // namespace OptimizeSampLoops

#else

typedef int16_t ped_id_t;
#	define INVALID_PED_ID ( ( ped_id_t ) - 1 )

DLL_PUBLIC ped_id_t OptimizeSampLoops__get_ped_id( void *ped );

#endif